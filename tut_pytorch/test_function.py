# -*- coding: utf-8 -*-
"""
@author: Joe
"""

import pytest
import torch
import torch.autograd as autograd
import torch.nn as nn
import torch.nn.functional as functional
import torch.optim as optim

torch.manual_seed(1)


def test_affine():

    # lin maps from R^5 to R^3, parameters A, b (f = A*x + b)
    lin = nn.Linear(5, 3)
    # data is 2x5
    data = autograd.Variable(torch.randn(2, 5))
    # out is 2x3
    print(lin(data))


def test_nonlinearity():

    # In pytorch, most non-linearities are in torch.functional (we have it imported as F)
    # Note that non-linearites typically don't have parameters like affine maps do.
    # That is, they don't have weights that are updated during training.
    data = autograd.Variable(torch.randn(2, 2))
    print(data)

    print("Sigmoid")
    print(functional.sigmoid(data))
    print("Tanh")
    print(functional.tanh(data))
    print("ReLU") # = max(0, x) = step(x)*x
    print(functional.relu(data))


def test_softmax():

    # Softmax is also in torch.nn.functional
    data = autograd.Variable(torch.randn(5))
    print(data)

    print("Softmax")
    print(functional.softmax(data, dim=0))
    assert functional.softmax(data, dim=0).data.sum() == pytest.approx(1)

    print("Log softmax")
    print(functional.log_softmax(data, dim=0))
