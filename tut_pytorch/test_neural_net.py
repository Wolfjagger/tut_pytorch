# -*- coding: utf-8 -*-
"""
@author: Joe
"""

import torch
import torch.autograd as autograd
import torch.nn as nn
import torch.nn.functional as functional
import torch.optim as optim


class Net(nn.Module):

    def __init__(self):
        super(Net, self).__init__()
        # 1 input image channel, 6 output channels, 5x5 square convolution
        # kernel
        self.conv1 = nn.Conv2d(1, 6, 5)
        self.conv2 = nn.Conv2d(6, 16, 5)
        # an affine operation: y = Wx + b
        self.fc1 = nn.Linear(16 * 5 * 5, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        # Max pooling over a (2, 2) window
        x = functional.max_pool2d(functional.relu(self.conv1(x)), (2, 2))
        # If the size is a square you can only specify a single number
        x = functional.max_pool2d(functional.relu(self.conv2(x)), 2)
        x = x.view(-1, self.num_flat_features(x))
        x = functional.relu(self.fc1(x))
        x = functional.relu(self.fc2(x))
        x = self.fc3(x)
        return x

    def num_flat_features(self, x):
        size = x.size()[1:]  # all dimensions except the batch dimension
        num_features = 1
        for s in size:
            num_features *= s
        return num_features


def test_net():

    net = Net()
    print(net)

    params = list(net.parameters())
    print(len(params))
    print(params[0])

    in_var = autograd.Variable(torch.randn(1, 1, 32, 32))

    out = net(in_var)
    print(out)

    net.zero_grad()

    out.backward(torch.randn(1, 10))


def test_loss():

    net = Net()
    in_var = autograd.Variable(torch.randn(1, 1, 32, 32))

    out = net(in_var)
    target = autograd.Variable(torch.arange(1, 11))
    target = target.view(1, -1)
    criterion = nn.MSELoss()

    loss = criterion(out, target)

    print("Loss functions:")
    fn = loss.grad_fn
    print(fn)
    while hasattr(fn, 'next_functions') and len(fn.next_functions) > 0:
        fn = fn.next_functions[0][0]
        print(fn)

    net.zero_grad()

    loss.backward()

    print('conv1.bias.grad after backward')
    print(net.conv1.bias.grad)

def test_learn():

    net = Net()
    in_var = autograd.Variable(torch.randn(1, 1, 32, 32))

    out = net(in_var)
    target = autograd.Variable(torch.arange(1, 11))
    target = target.view(1, -1)
    criterion = nn.MSELoss()

    loss = criterion(out, target)
    print("before update")
    print(loss)

    net.zero_grad()
    loss.backward()

    # Manually doing Stochastic Gradient Descent (SGD)
    learning_rate = 0.01
    for f in net.parameters():
        f.data.sub_(f.grad.data * learning_rate)

    out = net(in_var)
    loss = criterion(out, target)
    net.zero_grad()
    loss.backward()
    print("after update")
    print(loss)

    for _ in range(100):
        for f in net.parameters():
            f.data.sub_(f.grad.data * learning_rate)
        out = net(in_var)
        loss = criterion(out, target)
        net.zero_grad()
        loss.backward()

    print("after 100 updates")
    print(loss)


def test_auto_learn():

    net = Net()
    in_var = autograd.Variable(torch.randn(1, 1, 32, 32))

    # Using the optim package
    # create your optimizer
    optimizer = optim.SGD(net.parameters(), lr=0.01)

    target = autograd.Variable(torch.arange(1, 11))
    target = target.view(1, -1)
    criterion = nn.MSELoss()

    # in your training loop:
    optimizer.zero_grad()   # zero the gradient buffers
    out = net(in_var)
    loss = criterion(out, target)
    print("before update")
    print(loss)
    loss.backward()
    optimizer.step()    # Does the update

    optimizer.zero_grad()   # zero the gradient buffers
    out = net(in_var)
    loss = criterion(out, target)
    loss.backward()
    print("after update")
    print(loss)

    for _ in range(100):
        optimizer.zero_grad()
        output = net(in_var)
        loss = criterion(output, target)
        loss.backward()
        optimizer.step()

    print("after 100 updates")
    print(loss)
