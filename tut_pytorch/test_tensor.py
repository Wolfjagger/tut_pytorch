# -*- coding: utf-8 -*-
"""
@author: Joe
"""

from copy import deepcopy

import pytest
import torch
import torch.autograd as autograd


def test_basic_tensor():

    x = torch.rand(5, 3)
    y = torch.rand(5, 3)

    z = x + y

    z1 = torch.add(x, y)
    assert torch.equal(z, z1)
    z2 = torch.Tensor(5, 3)
    torch.add(x, y, out=z2)
    assert torch.equal(z, z2)
    z3 = deepcopy(z)
    assert torch.equal(z, z3)
    z4 = deepcopy(x)
    z4.add_(y)
    assert torch.equal(z, z4)


def test_basic_autograd():

    x = autograd.Variable(torch.ones(2, 2), requires_grad=True)

    y = x + 2
    z = y * y * 3
    out = z.mean()

    out.backward()
    print(x.grad)
    assert x.grad.data.equal((2 * 3*(x.data + 2))/4)


def test_runtime_autograd():

    x = torch.Tensor([4, 2, 0])
    x = autograd.Variable(x, requires_grad=True)

    y = x
    loop = 1
    while 0.001 < y.data.norm() < 1000:
        loop = loop + 1
        y = y * x

    print(y)

    gradients = torch.Tensor([[2, 4, 8], [4, 2, 1]])
    y.backward(gradients)
    print(x.grad)
    assert x.grad.data.equal(gradients * loop*x.data**(loop-1))


def test_concat():

    # By default, it concatenates along the first axis (concatenates rows)
    x_1 = torch.randn(2, 5)
    y_1 = torch.randn(3, 5)
    z_1 = torch.cat([x_1, y_1])
    assert z_1.size() == (5, 5)

    # Concatenate columns:
    x_2 = torch.randn(2, 3)
    y_2 = torch.randn(2, 5)
    # second arg specifies which axis to concat along
    z_2 = torch.cat([x_2, y_2], 1)
    assert z_2.size() == (2, 8)

    # If your tensors are not compatible, torch will complain.
    with pytest.raises(RuntimeError):
        torch.cat([x_1, x_2])


def test_view():

    x = torch.randn(2, 3, 4)
    assert x.size() == (2, 3, 4)
    assert x.view(2, 12).size() == (2, 12)  # Reshape to 2 rows, 12 columns
    assert x.view(3, -1).size() == (3, 8) # If one of the dimensions is -1, its size can be inferred
