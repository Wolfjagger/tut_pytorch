# -*- coding: utf-8 -*-
"""
@author: Joe
Based on http://pytorch.org/tutorials/beginner/nlp/word_embeddings_tutorial.html
Original author: Robert Guthrie
"""

import torch
import torch.autograd as autograd
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

torch.manual_seed(1)


def test_simple_embedding():

    ix_to_word = dict(enumerate(["hello", "world"]))
    word_to_ix = {val: key for key, val in ix_to_word.items()}
    embeds = nn.Embedding(2, 5)  # 2 words in vocab, 5 dimensional embeddings
    lookup_tensor = torch.LongTensor([word_to_ix["hello"]])
    hello_embed = embeds(autograd.Variable(lookup_tensor))
    print(hello_embed)



class NGramLanguageModeler(nn.Module):

    def __init__(self, vocab_size, embedding_dim, context_size):
        super(NGramLanguageModeler, self).__init__()
        self.embeddings = nn.Embedding(vocab_size, embedding_dim)
        self.linear1 = nn.Linear(context_size * embedding_dim, 128)
        self.linear2 = nn.Linear(128, vocab_size)

    def forward(self, inputs):
        embeds = self.embeddings(inputs).view((1, -1))
        out = F.relu(self.linear1(embeds))
        out = self.linear2(out)
        log_probs = F.log_softmax(out, dim=1)
        return log_probs


def test_ngram():

    context_size = 2
    embedding_dim = 10

    # We will use Shakespeare Sonnet 2
    test_sentence = "\n".join((
        "When forty winters shall besiege thy brow,",
        "And dig deep trenches in thy beauty's field,",
        "Thy youth's proud livery so gazed on now,",
        "Will be a totter'd weed of small worth held:",
        "Then being asked, where all thy beauty lies,",
        "Where all the treasure of thy lusty days;",
        "To say, within thine own deep sunken eyes,",
        "Were an all-eating shame, and thriftless praise.",
        "How much more praise deserv'd thy beauty's use,",
        "If thou couldst answer 'This fair child of mine",
        "Shall sum my count, and make my old excuse,'",
        "Proving his beauty by succession thine!",
        "This were to be new made when thou art old,",
        "And see thy blood warm when thou feel'st it cold."
    )).split()

    # We should tokenize the input, but we will ignore that for now
    # Build a list of tuples.  Each tuple is ([ word_i-2, word_i-1 ], target word)
    trigrams = [
        (
            test_sentence[i-context_size:i],
            test_sentence[i]
        )
        for i in range(context_size, len(test_sentence))
    ]

    # Print the first 3, just so you can see what they look like
    print(trigrams[0])

    vocab = set(test_sentence)
    word_to_ix = {word: i for i, word in enumerate(vocab)}


    losses = []
    loss_function = nn.NLLLoss()
    model = NGramLanguageModeler(len(vocab), embedding_dim, context_size)
    optimizer = optim.SGD(model.parameters(), lr=0.001)

    for epoch in range(10):
        total_loss = torch.Tensor([0])
        for context, target in trigrams:

            # Step 1. Prepare the inputs to be passed to the model (i.e, turn the words
            # into integer indices and wrap them in variables)
            context_idxs = [word_to_ix[w] for w in context]
            context_var = autograd.Variable(torch.LongTensor(context_idxs))

            # Step 2. Recall that torch *accumulates* gradients. Before passing in a
            # new instance, you need to zero out the gradients from the old
            # instance
            model.zero_grad()

            # Step 3. Run the forward pass, getting log probabilities over next
            # words
            log_probs = model(context_var)

            # Step 4. Compute your loss function. (Again, Torch wants the target
            # word wrapped in a variable)
            loss = loss_function(log_probs, autograd.Variable(
                torch.LongTensor([word_to_ix[target]])))

            # Step 5. Do the backward pass and update the gradient
            loss.backward()
            optimizer.step()

            total_loss += loss.data
        losses.append(total_loss)
    print(losses)  # The loss decreased every iteration over the training data!



class CBOW(nn.Module):

    def __init__(self, vocab_size, embedding_dim, context_size):
        super().__init__()

        self.embedding_dim = embedding_dim
        self.context_size = context_size
        self.vocab_size = vocab_size

        self.embeddings = nn.Embedding(vocab_size, embedding_dim)

        # Define the parameters that you will need.  In this case, we need A and b,
        # the parameters of the affine mapping.
        # Torch defines nn.Linear(), which provides the affine map.
        # Make sure you understand why the input dimension is vocab_size
        # and the output is num_labels!
        self.linear = nn.Linear(self.embedding_dim*(2*self.context_size), self.vocab_size)

        # NOTE! The non-linearity log softmax does not have parameters! So we don't need
        # to worry about that here

    def forward(self, inputs):
        
        embeds = self.embeddings(inputs).view((1, -1))
        out = self.linear(embeds)
        log_probs = F.log_softmax(out, dim=1)
        return log_probs


def test_continuous_bag_of_words():

    embedding_dim = 10
    context_size = 2  # 2 words to the left, 2 to the right
    raw_text = """We are about to study the idea of a computational process.
    Computational processes are abstract beings that inhabit computers.
    As they evolve, processes manipulate other abstract things called data.
    The evolution of a process is directed by a pattern of rules
    called a program. People create programs to direct processes. In effect,
    we conjure the spirits of the computer with our spells.""".split()

    # By deriving a set from `raw_text`, we deduplicate the array
    vocab = set(raw_text)
    vocab_size = len(vocab)

    word_to_ix = {word: i for i, word in enumerate(vocab)}
    data = []
    for i in range(2, len(raw_text) - 2):
        context = [raw_text[i - 2], raw_text[i - 1],
                raw_text[i + 1], raw_text[i + 2]]
        target = raw_text[i]
        data.append((context, target))
    print(data[:5])

    def make_context_vector(context, word_to_ix):
        idxs = [word_to_ix[w] for w in context]
        tensor = torch.LongTensor(idxs)
        return autograd.Variable(tensor)

    print(make_context_vector(data[0][0], word_to_ix))  # example


    losses = []
    loss_function = nn.NLLLoss()
    model = CBOW(vocab_size, embedding_dim, context_size)
    optimizer = optim.SGD(model.parameters(), lr=0.001)

    for epoch in range(10):
        total_loss = 0
        for context, target in data:

            # Step 1. Prepare the inputs to be passed to the model (i.e, turn the words
            # into integer indices and wrap them in variables)
            context_var = make_context_vector(context, word_to_ix)

            # Step 2. Recall that torch *accumulates* gradients. Before passing in a
            # new instance, you need to zero out the gradients from the old
            # instance
            model.zero_grad()

            # Step 3. Run the forward pass, getting log probabilities
            log_probs = model(context_var)

            # Step 4. Compute your loss function. (Again, Torch wants the target
            # word wrapped in a variable)
            loss = loss_function(log_probs, autograd.Variable(
                torch.LongTensor([word_to_ix[target]])))

            # Step 5. Do the backward pass and update the gradient
            loss.backward()
            optimizer.step()

            total_loss += loss.data[0]
        losses.append(total_loss)
    print(losses)  # The loss decreased every iteration over the training data!
